package com.example.monbonsai

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.monbonsai.fragments.AddPlantFragment
import com.example.monbonsai.fragments.CollectionFragment
import com.example.monbonsai.fragments.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragment(HomeFragment(this), R.string.homePage_title)

        val navigationView = findViewById<BottomNavigationView>(R.id.navigation_view)
        navigationView.setOnNavigationItemSelectedListener { it ->
            when(it.itemId)
            {
                R.id.Homepage -> {
                    loadFragment(HomeFragment(this), R.string.homePage_title)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.collection_page -> {
                    loadFragment(CollectionFragment(this), R.string.collectionPage_title)
                    return@setOnNavigationItemSelectedListener true

                }
                R.id.add_page -> {
                    loadFragment(AddPlantFragment(this), R.string.addPage_title)
                    return@setOnNavigationItemSelectedListener true

                }
                else->false
            }
        }


    }

    private fun loadFragment(fragment: Fragment, string: Int) {
        //charger notre plantRepository
        val repo = PlantRepository()

        findViewById<TextView>(R.id.page_title).text = resources.getString(string)
        //mettre a jour la liste de plante
        repo.updateData{
            //injecter le fragment dans notre boite(fragment_container)
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}