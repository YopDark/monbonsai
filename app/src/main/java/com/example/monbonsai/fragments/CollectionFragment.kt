package com.example.monbonsai.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.monbonsai.MainActivity
import com.example.monbonsai.PlantRepository.Singleton.plantList
import com.example.monbonsai.R
import com.example.monbonsai.adapter.PlantAdapter
import java.security.AccessControlContext

class CollectionFragment(
    val context: MainActivity
): Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater?.inflate(R.layout.fragment_collection, container, false)

        //Récuperer le recyclerView
        val collectionRecyclerView = view.findViewById<RecyclerView>(R.id.collection_recycler_list)
        collectionRecyclerView.adapter = PlantAdapter(context, plantList.filter { it.liked }, R.layout.item_vertical)
        collectionRecyclerView.layoutManager = LinearLayoutManager(context)
        return view
    }
}