package com.example.monbonsai.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.monbonsai.MainActivity
import com.example.monbonsai.PlantModel
import com.example.monbonsai.PlantRepository.Singleton.plantList
import com.example.monbonsai.R
import com.example.monbonsai.adapter.PlantAdapter

class HomeFragment(
    private val context: MainActivity
) : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_home, container, false )

        /* LOCAL SANS BDD
//Créer une liste qui va stocker ces plantes
        val plantList = arrayListOf<PlantModel>()

        //Enregistrer une premiere plante dans la liste
        plantList.add(
            PlantModel(
            "Juniperus",
            "Genévrier",
            "https://www.bonsaiempire.fr/images/cheap-juniper-bonsai.jpg",
                false
        )
        )

        //Enregistrer une deuxieme plante dans la liste
        plantList.add(
            PlantModel(
            "Ulmus parvifolia",
            "Orme de chine",
            "https://www.bonsaiempire.fr/images/stories/species/elm-ulmus-parvifolia.jpg",
                true
        )
        )

        //Enregistrer une troisieme plante dans la liste
        plantList.add(
            PlantModel(
            "Pinus",
            "Pin Bonsaï",
            "https://bonsaimontreal.com/wp-content/uploads/2019/01/Pin-blanc.jpg",
                false
        )
        )

        //Enregistrer une quatrieme plante dans la liste
        plantList.add(
            PlantModel(
            "Quercus",
            "Chene",
            "https://www.bonsaiempire.fr/images/stories/oak-bonsai-walterpall.jpg",
                true
        )
        )
        //Enregistrer une cinquieme plante dans la liste
        plantList.add(
            PlantModel(
            "Prunus serrulata",
            "Sakura",
            "https://cdna.artstation.com/p/assets/images/images/016/574/702/large/joe-petisce-tree-arts.jpg?1552655975",
                false
        )
        )*/

        //récuper le recyclerview
        val horizontalRecyclerView = view.findViewById<RecyclerView>(R.id.horizontal_recyclerview)
        horizontalRecyclerView.adapter = PlantAdapter(context, plantList.filter { !it.liked}, R.layout.item_horizontal)

        //récup le 2em recyclerview
        val verticalRecyclerView = view.findViewById<RecyclerView>(R.id.vertical_recyclerview)
        verticalRecyclerView.adapter = PlantAdapter(context, plantList, R.layout.item_vertical)


        return view
    }
}