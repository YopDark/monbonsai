package com.example.monbonsai.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import androidx.fragment.app.Fragment
import com.example.monbonsai.MainActivity
import com.example.monbonsai.PlantModel
import com.example.monbonsai.PlantRepository
import com.example.monbonsai.PlantRepository.Singleton.downloadUri
import com.example.monbonsai.R
import java.util.*

class AddPlantFragment(
    val context: MainActivity
): Fragment() {

    private var file: Uri? = null

    private var uploadedImage:ImageView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater?.inflate(R.layout.fragment_add_plant, container, false)

        //Récuperer uploadedImage pour lui associer sont composant
        uploadedImage = view.findViewById(R.id.image_preview)
        //Récuperer le button pour charger l'image
        val pickUpImageButton = view.findViewById<Button>(R.id.upload_image)

        //Lors du clic ca ouvre les image du telephone
        pickUpImageButton.setOnClickListener{pickUpImage()}

        //recuperer le button confirm
        val buttonConfirm = view.findViewById<Button>(R.id.confirm_button)
        buttonConfirm.setOnClickListener{ sendForm(view) }
        return view
    }

    private fun sendForm(view: View) {
        val repo = PlantRepository()
        repo.uploadImage(file!!){
            val plantName = view.findViewById<EditText>(R.id.name_input).text.toString()
            val plantDescription = view.findViewById<EditText>(R.id.description_input).text.toString()
            val plantGrow = view.findViewById<Spinner>(R.id.grow_spinner).selectedItem.toString()
            val plantWater = view.findViewById<Spinner>(R.id.water_spinner).selectedItem.toString()
            val downloadImageUri = downloadUri

            //creer un nouvel objet PlantModel
            val plant = PlantModel(
                UUID.randomUUID().toString(),
                plantName,
                plantDescription,
                downloadImageUri.toString(),
                plantGrow,
                plantWater

            )

            //envoyer en bdd
            repo.insertPlant(plant)
        }


    }

    private fun pickUpImage() {
        val intent = Intent()
        intent.type = "image/"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 47)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 47 &&resultCode == Activity.RESULT_OK){

            //Vérifier si les données sont null
            if(data == null || data.data == null) return

            //Récuperer l'image qui a été selectionner
            file = data.data

            //mettre a jour l'apercu de l'image
            uploadedImage?.setImageURI(file)


        }
    }
}