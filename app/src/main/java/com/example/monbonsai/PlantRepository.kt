package com.example.monbonsai

import android.net.Uri
import com.example.monbonsai.PlantRepository.Singleton.databaseRef
import com.example.monbonsai.PlantRepository.Singleton.downloadUri
import com.example.monbonsai.PlantRepository.Singleton.plantList
import com.example.monbonsai.PlantRepository.Singleton.storageReference
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import java.net.URI
import java.util.*


class PlantRepository {

    object Singleton{
        //Donner le lien pour acceder au bucket
        private val BUCKET_URL: String = "gs://monbonsai-94873.appspot.com"
        //Se connecter a notre espace de stockage
        val storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(BUCKET_URL)
        //Se connecter a la reference plants
        val databaseRef= FirebaseDatabase.getInstance().getReferenceFromUrl("https://monbonsai-94873-default-rtdb.firebaseio.com/bonsai")

        //créer une liste qui va contenir nos plantes
        val plantList = arrayListOf<PlantModel>()

        //Contenir le lien de l'image courante
        var downloadUri: Uri? =null
    }

    fun updateData(callback: () -> Unit){

        //absorber les données depuis la databaseRef -> liste de plantes
        databaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                //retirer les plante dans ma plantList
                plantList.clear()
                //recolter la liste
                for(ds in snapshot.children){
                    //construire un objet plante
                    val bonsai = ds.getValue(PlantModel::class.java)

                    //vérifier que la plante n'est pas null
                    if(bonsai != null){
                        //ajouter plant à notre liste
                        plantList.add(bonsai)
                    }
                }
                //appel du callback
                callback()
            }

            override fun onCancelled(error: DatabaseError) {}

        })
    }
    //Créer une fonction pour envoyer des fichier sur le storage
    fun uploadImage(file: Uri, callback: () -> Unit){

        if(file != null){
            val fileName = UUID.randomUUID().toString() + ".jpg"
            val ref = storageReference.child(fileName)
            val uploadTask = ref.putFile(file)

            //demarer la tache
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                //Si il y a eu un probleme lors de l'envoie du fichier

                if(!task.isSuccessful){
                    task.exception?.let{ throw it }
                }

                return@Continuation ref.downloadUrl
            }).addOnCompleteListener{ task->

                //Vérifier si tout a bien fonctionner
                if(task.isSuccessful){
                    //Récuperer l'image
                    downloadUri = task.result
                    callback()
                }
            }
        }
    }

    //mettre a jour l'objet plant dans la bdd
    fun updatePlant(plant: PlantModel) = databaseRef.child(plant.id).setValue(plant)

    //insrer l'objet plant dans la bdd
    fun insertPlant(plant: PlantModel) = databaseRef.child(plant.id).setValue(plant)



    //supprier un bonsai de la base
    fun deletePlant(plant: PlantModel) = databaseRef.child(plant.id).removeValue()
}