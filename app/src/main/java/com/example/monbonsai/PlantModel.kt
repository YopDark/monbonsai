package com.example.monbonsai

class PlantModel(

    val id: String = "bonsai0",
    val name: String = "Acer Palmatum",
    val description: String = "Erable du Japon",
    val imageURL: String = "https://cdn.pixabay.com/photo/2021/03/22/11/40/bonsai-6114252_960_720.jpg",
    val grow: String = "Lente",
    val water: String = "Faible",
    var liked: Boolean = false,
    val rempotage: String = "03/10/2021",
    val engrais: String = "03/10/2021"
) {
}