package com.example.monbonsai

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.monbonsai.adapter.PlantAdapter

class PlantPopup(private val adapter: PlantAdapter, private val currentPlant: PlantModel): Dialog(adapter.context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.popup_plant_detail)
        setupComponents()
        setupCloseButton()
        setupDeleteButton()
        setupStarButton()
    }

    private fun updateStar(button: ImageView){
        if(currentPlant.liked){
            button.setImageResource(R.drawable.ic_star)
        }else{
            button.setImageResource(R.drawable.ic_unstar)
        }
    }

    private fun setupStarButton() {
        //récup
        val starButton = findViewById<ImageView>(R.id.likedButton)

        updateStar(starButton)

        //interaction
        starButton.setOnClickListener{
            currentPlant.liked = !currentPlant.liked
            val repo = PlantRepository()
            repo.updatePlant(currentPlant)
            updateStar(starButton)
        }
    }

    private fun setupDeleteButton() {
        findViewById<ImageView>(R.id.deleteButton).setOnClickListener{
            //Supprimer le bonsai de la bdd
            val repo = PlantRepository()
            repo.deletePlant(currentPlant)
            dismiss()
        }
    }

    private fun setupCloseButton() {
        findViewById<ImageView>(R.id.closeButton).setOnClickListener{
            //fermer la popup
            dismiss()
        }
    }

    private fun setupComponents() {
        //actualiser l'image du bonsai
        val bonsaiImage = findViewById<ImageView>(R.id.image_item)
        Glide.with(adapter.context).load(Uri.parse(currentPlant.imageURL)).into(bonsaiImage)

        //actualiser le nom
        findViewById<TextView>(R.id.popUp_namePlant_title).text = currentPlant.name

        //actualiser la description
        findViewById<TextView>(R.id.popUp_desciptionPlant).text = currentPlant.description

        //actualiser la croissance
        findViewById<TextView>(R.id.popUp_growthPlant).text = currentPlant.grow

        //actualiser la conssomation d'eau
        findViewById<TextView>(R.id.popUp_consumptionPlant).text = currentPlant.water


    }
}