package com.example.monbonsai.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.monbonsai.*


class PlantAdapter(
    val context: MainActivity,
    private val plantList : List<PlantModel>,
    private val layoutId : Int
    ) : RecyclerView.Adapter<PlantAdapter.ViewHolder>(){

    //Boite pour ranger tous les composant à controler
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        //image de la plante?
        val plantImage = view.findViewById<ImageView>(R.id.image_item)
        val plantName:TextView? = view.findViewById(R.id.item_name)
        val plantDescription:TextView? = view.findViewById(R.id.item_description)
        val starIcon = view.findViewById<ImageView>(R.id.star_icon)

    }

    //Injecter notre layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(layoutId, parent, false)

        return ViewHolder(view)
    }

    //Mettre a jour chaque model avec notre plante
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //recuperer les info de la plante
        val currentPlant = plantList[position]

        //récuperer le repository
        val repo = PlantRepository()

        //Utiliser Glide pour récuperer l'image à partir de sont lien -> composant
        Glide.with(context).load(Uri.parse(currentPlant.imageURL)).into(holder.plantImage)

        //Mettre à jour le nom de la plante
        holder.plantName?.text = currentPlant.name
        //Mettre à jour la description de la plante
        holder.plantDescription?.text = currentPlant.description
        //liké ou non
        if (currentPlant.liked){
            holder.starIcon.setImageResource(R.drawable.ic_star)
        }
        else{
            holder.starIcon.setImageResource(R.drawable.ic_unstar)

        }

        //rejouter une interaction avec le like
        holder.starIcon.setOnClickListener{
            //inverser si le bouton est like ou non
            currentPlant.liked = !currentPlant.liked
            //mettre a jour l'objet plant
            repo.updatePlant(currentPlant)
        }

        //interaction lors du clic sur un bonsai
        holder.itemView.setOnClickListener{
            //afficher la popup
            PlantPopup(this, currentPlant).show()
        }
    }

    //Renvoyer combien d'item dynamiquement
    override fun getItemCount(): Int = plantList.size
}